import pandas as pd
import json
import pygal

# df1 = user_transactions
# df2 = user_poll

df1 = pd.read_csv('user_transactions.csv')

# remove outliers(amount zero means they unsubscribe from the plan?)
df1 = df1.loc[df1.amount != 0]

df1['transaction_time'] = pd.to_datetime(df1['transaction_time'], format='%Y-%m-%d %H:%M:%S')
df2 = pd.read_csv('user_poll.csv')
df2['poll'] = df2.poll.apply(json.loads)
df2['role'] = df2.poll.apply(lambda x: x['role'])

# inner join user_transaction and user_poll on user id column
df = pd.merge(df1, df2[['user', 'role']], on='user', sort=False)

df['period'] = df.transaction_time.dt.strftime(date_format='%Y-%m')
result = df.groupby(['role', 'period']).size().to_frame('number_of_subscribers').reset_index()
pivot_table = result.pivot(index='role', columns='period', values='number_of_subscribers')

# plotting
line_chart = pygal.Line()
line_chart.title = 'Role subscription evolution(number of subscribers)'
line_chart.x_labels = pivot_table.columns.values
for role in pivot_table.index.values:
    line_chart.add(role, pivot_table.loc[role])
line_chart.render_to_file('/vagrant/analysis/v.svg')
