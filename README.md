
> We ask you to manage the following tasks in a clear and detailed way, providing both the code that you used to analyse the datasets as well as the images (or any other support) for the visualisations. Although you are free to use the tools you are familiar with, we strongly encourage you to share your answers through a unique Jupyter notebook.

> Keep in mind that quality and clarity are our main criteria for evaluating your test. The communication of information is a major part of the analyst role and will be assessed from the comments of both your analysis and code.

The user journey in Piktochart can be split into two phases: an exploration phase during which the person signs up and discovers the potential of the tool and an exploitation phase after the user subscribes to one of the available plans. The BackEnd team has extracted for you the subscribers to the different plans, `user_transactions.csv`, and the "job" position of the users, `user_poll.csv`, but did not provide a detailed explanation of both contents. It also computed the total number of users per role over the period of interest in `poll_statistics.csv`.

## Making the data sets yours...

After describing the content of the data sets, your task is to grasp a better perspective on the user history of transactions by building the sequence of payments, highlighting their main features as well as the number of people for each. You will in particular explain your understanding of the transactions labels.

This information is important to shape the focus of the company. As the labelling is particularly verbose though, you need to design a scheme for sharing the sequences in an intelligible way.

## ...before sharing with the teams

The head of the finance team asks for your help in highlighting both the share of spending and the number of subscribers for each role. To do so, your task is to design charts that would exhibit their evolution over time across roles.

As you share the visualisation with the rest of the company, the head of marketing decides to base the future campaigns by targeting the roles which have the most subscribers so far:
- Do you think that this decision is sound considering the current content of your analysis?
- What could be the limitations of the previous metrics (number of subscribers) regarding the targeted roles?
- What other metrics, if any, would you have highlighted to the head of the marketing team in focusing on the right roles? Explain your rationale to support and provide computation and/or visualisation if applicable. Highlight potential missing data to extract other relevant information.
